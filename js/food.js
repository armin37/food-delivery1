const foods = [
    {id: 1, name: 'همبرگر', processTime: 20, icon: 'assets/burger/1.jpg'},
    {id: 2, name: 'ماشروم برگر', processTime: 30, icon: 'assets/burger/2.jpg'},
    {id: 3, name: 'چیز همبرگر', processTime: 40, icon: 'assets/burger/3.jpg'},
    {id: 4, name: 'سینی برگر', processTime: 40, icon: 'assets/burger/4.jpg'},
    {id: 5, name: 'پیتزا چیکن', processTime: 50, icon: 'assets/pizza/2.jpg'},
    {id: 6, name: 'مارگاریتا', processTime: 60, icon: 'assets/pizza/3.jpg'},
    {id: 7, name: 'قیمه', processTime: 60, icon: 'assets/irani/قیمه.jpg'},
    {id: 8, name: 'قورمه سیزی', processTime: 70, icon: 'assets/irani/قورمه.jpg'}
];

const cart = [];


(function () {
    foods.forEach((value, index) => {
        document.getElementById('food-select').appendChild(new Option(value.name, value.id))
    });

    const randomFoodOver = Math.floor(Math.random() * 8);
    foods[randomFoodOver].isOver = true;

    document.getElementById('food-select').addEventListener('change', checkFoodAvailability)
    document.getElementById('food-count-input').addEventListener('change', checkFoodAvailability)
    document.getElementById('add-to-cart-btn').addEventListener('click', addFoodToCart)
    document.getElementById('processing-foods').addEventListener('click', alertProcessingFoods)
    document.getElementById('delivered-foods').addEventListener('click', alertDeliveredFoods)
})();

function checkFoodAvailability() {
    const select = document.getElementById('food-select');
    const input = document.getElementById('food-count-input');
    const btn = document.getElementById('add-to-cart-btn');
    select.disabled = true;
    input.disabled = true;
    btn.disabled = true;
    btn.innerText = 'افزودن';

    btn.classList.add('button-loading');
    setTimeout(() => {
        select.disabled = false;
        input.disabled = false;
        btn.disabled = false;
        btn.classList.remove('button-loading');
        const index = foods.findIndex(item => item.id === +select.value)
        if (foods[index].isOver) {
            btn.disabled = true;
            btn.innerText = 'تمام شده';
        }
    }, 1500)
}

function addFoodToCart() {
    const select = document.getElementById('food-select');
    const input = document.getElementById('food-count-input');
    const selectedFood = foods.find(item => item.id === +select.value)
    if (selectedFood.isOver) {
        alert('این غدا تمام شده');
        return;
    }

    if (parseInt(input.value) < 1) {
        alert('تعداد باید بیشتر از 1 باشد')
        return;
    }
    selectedFood.count = parseInt(input.value);

    const cartItem = cart.find(item => item.id === +select.value)
    if (cartItem) {
        cartItem.count = parseInt(input.value);
        const spanCount = document.getElementById('food' + selectedFood.id).getElementsByTagName('span')[0];
        spanCount.innerText = cartItem.count.toString();
        return;
    }

    const processingFoods = document.getElementById('processing-foods');
    const col = document.createElement("div");
    col.classList.add('col');
    const div = document.createElement("div");
    div.classList.add('food');
    div.id = 'food' + selectedFood.id;
    const img = document.createElement("img");
    img.src = selectedFood.icon;
    const name = document.createElement("p");
    name.innerText = selectedFood.name
    const count = document.createElement("span");
    count.innerText = input.value
    div.appendChild(img);

    const foodInfo = document.createElement("div");
    foodInfo.classList.add('food-info');
    foodInfo.appendChild(name);
    foodInfo.appendChild(count);
    const countDown = document.createElement("div");
    countDown.innerText = selectedFood.processTime;
    countDown.id = 'counter' + selectedFood.id;
    foodInfo.appendChild(countDown);
    div.appendChild(foodInfo);
    col.appendChild(div);
    processingFoods.appendChild(col);
    cart.push(selectedFood);

    setInterval(() => {
        const count = document.getElementById('counter' + selectedFood.id);
        if (count.innerText > 0) {
            const cartItem = cart.find(item => item.id === +selectedFood.id);
            cartItem.processTime--;
            count.innerText = (parseInt(count.innerText) - 1).toString();
        } else {
            count.innerText = '';
            processingFoods.removeChild(col);
            const deliveredFoods = document.getElementById('delivered-foods');
            deliveredFoods.appendChild(col);
        }
    }, 1000);
}

function alertProcessingFoods() {
    const itemWithMaxProcessTime = cart.reduce((prev, current) => (prev.processTime > current.processTime) ? prev : current)
    alert(`All foods will be delivered in ${itemWithMaxProcessTime.processTime} seconds`);
}

function alertDeliveredFoods() {
    alert('Delivered foods section clicked');
}
